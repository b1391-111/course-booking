import React, { Fragment, useState } from 'react';

// react-bootstrap
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Modal, 
    Row, 
    Table 
} from 'react-bootstrap';

// alert
import Swal from 'sweetalert2';

const AdminView = ({ courses, fetchedData }) => {
    const [show, setShow] = useState(false);
    const [cId, setCId] = useState("");
    const [formData, setFormData] = useState({
        courseName: "",
        courseDesc: "",
        price: 0
    });

    // event handlers
    const handleShow = (isCreate, courseId) => {
        if(isCreate) {
            setFormData({
                courseName: "",
                courseDesc: "",
                price: 0
            })
            setCId("");
        } else {
            setCId(courseId);
            fetch(`https://sheltered-reaches-76330.herokuapp.com/api/courses/${courseId}`)
            .then(res => res.json())
            .then(res => {
                setFormData(res);
            })
        }
        setShow(true)
    };
    
    const handleClose = () => {
        setFormData({
            courseName: "",
            courseDesc: "",
            price: 0
        })
        setShow(false)
    };

    const handleChange = (e) => setFormData({...formData, [e.target.name]: e.target.value});

    const addCourse = (e) => {
        e.preventDefault();

        const { courseName, courseDesc, price } = formData;
        let newPrice = parseInt(price);
        const newCourse = {
            courseName,
            courseDesc,
            price: newPrice
        };

        fetch("https://sheltered-reaches-76330.herokuapp.com/api/courses/create-course", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization" : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify(newCourse)
        })
            .then(res => res.json())
            .then(res => {
                if(res.bool){
                    new Swal({
                        title: "Successfully Created",
                        icon: "success",
                        text: res.message
                    })
                    fetchedData();
                    handleClose();
                } else {
                    new Swal({
                        title: "Error",
                        icon: "error",
                        text: res.message
                    });
                }
            })
    }

    const editCourse = (e) => {
        e.preventDefault();

        const { courseName, courseDesc, price } = formData;
        let newPrice = parseInt(price);
        const updatedCourse = {
            courseName,
            courseDesc,
            price: newPrice
        };

        fetch(`https://sheltered-reaches-76330.herokuapp.com/api/courses/${cId}/update-course`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization" : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify(updatedCourse)
        })
            .then(res => res.json())
            .then(res => {
                if(res.bool){
                    new Swal({
                        title: "Success",
                        icon: "success",
                        text: res.message
                    })
                    fetchedData();
                    handleClose();
                } else {

                }
            })
    }

    const updateAvailability = (courseId) => {
        fetch(`https://sheltered-reaches-76330.herokuapp.com/api/courses/${courseId}/update-course-active`, {
            method: "PUT"
        })
            .then(res => res.json())
            .then(res => {
                if(res.bool){
                    new Swal({
                        title: "Success",
                        icon: "success",
                        text: res.message
                    })
                    fetchedData();
                } else {
                    new Swal({
                        title: "There's an error",
                        icon: "error",
                        text: res.message
                    })
                }
            })
    }

    const thousands_separators = (num) => {
        let num_parts = num.toString().split(".");
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return num_parts.join(".");
    }

    return (
        <Container>
            <h2 className="text-center"> Admin Dashboard </h2>
            <Row className="justify-content-center mb-3">
                <Col>
                    <div className='text-right'>
                        <Button 
                            variant="info"
                            className=" text-right"
                            onClick={() => {handleShow(true)}}
                        >
                            Add new Course
                        </Button>

                        {/* ADD COURSE */}
                        <Modal 
                            size="lg"
                            show={show} 
                            onHide={handleClose}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title className="text-center">
                                    Add New Course
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    <Form.Group controlId="courseName">
                                        <Form.Label>Course Name</Form.Label>
                                        <Form.Control 
                                            type="text"
                                            name="courseName"
                                            onChange={handleChange}
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="courseDesc">
                                        <Form.Label>Course Description</Form.Label>
                                        <Form.Control 
                                            type="text"
                                            name="courseDesc"
                                            onChange={handleChange}
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="price">
                                        <Form.Label>Course Price</Form.Label>
                                        <Form.Control 
                                            type="number"
                                            name="price"
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="info" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button variant="info" onClick={addCourse}>
                                    Submit
                                </Button>
                            </Modal.Footer>
                        </Modal>

                        {/* EDIT COURSE */}
                        <Modal 
                            size="lg"
                            show={show} 
                            onHide={handleClose}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title className="text-center">
                                    Edit Course
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    <Form.Group controlId="courseName">
                                        <Form.Label>Course Name</Form.Label>
                                        <Form.Control 
                                            type="text"
                                            name="courseName"
                                            onChange={handleChange}
                                            value={formData.courseName}
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="courseDesc">
                                        <Form.Label>Course Description</Form.Label>
                                        <Form.Control 
                                            type="text"
                                            name="courseDesc"
                                            onChange={handleChange}
                                            value={formData.courseDesc}
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="price">
                                        <Form.Label>Course Price</Form.Label>
                                        <Form.Control 
                                            type="number"
                                            name="price"
                                            onChange={handleChange}
                                            value={formData.price}
                                        />
                                    </Form.Group>
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="info" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button variant="info" onClick={editCourse}>
                                    Update
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </div>
                </Col>
            </Row>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Enrolled</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        courses.map(course => {
                            return (
                                <tr key={course._id}>
                                    <td>{course.courseName}</td>
                                    <td>{course.courseDesc}</td>
                                    <td>₱{thousands_separators(course.price)}</td>
                                    <td>{course.isActive ? "Available" : "Unavailable"}</td>
                                    <td>{course.enrollees.length}</td>
                                    <td>
                                        <>
                                            <Button
                                                variant="secondary"
                                                onClick={() => {handleShow(false, course._id)}}
                                            >
                                                Edit
                                            </Button>
                                            {
                                                course.isActive ? (
                                                    <>
                                                        <Button
                                                            variant="warning"
                                                            className="m-3"
                                                            onClick={() => {updateAvailability(course._id)}}
                                                        >
                                                            Archive
                                                        </Button>
                                                    </>
                                                ) : (
                                                    <>
                                                        <Button
                                                            variant="success"
                                                            className="m-3"
                                                            onClick={() => {updateAvailability(course._id)}}
                                                        >
                                                            Activate
                                                        </Button>
                                                        <Button
                                                            variant="danger"
                                                        >
                                                            Delete
                                                        </Button>
                                                    </>
                                                )
                                            }
                                        </>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
        </Container>
    )
}

export default AdminView
