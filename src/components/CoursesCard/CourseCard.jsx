import React from 'react';

// react-bootstrap component
import { Card } from 'react-bootstrap';

// react router
import { Link } from 'react-router-dom';

const CourseCard = ({_id, title, desc, price }) => {

    const thousands_separators = (num) => {
        let num_parts = num.toString().split(".");
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return num_parts.join(".");
    }

    return (
        <Card
            key={_id}
            className="text-left"
            style={{ 
                margin: "10px", 
                height: "250px"
            }}
        >
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-dark">
                    Description: 
                    <p> {desc} </p>
                </Card.Subtitle>
                <Card.Text>
                    Price:
                    <Card.Text>PHP {thousands_separators(price)}</Card.Text>
                </Card.Text>
                <Link
                    className='btn btn-info'
                    to={`/courses/${_id}`}
                >
                    Details
                </Link>
            </Card.Body>
        </Card>
    )
}

export default CourseCard;
