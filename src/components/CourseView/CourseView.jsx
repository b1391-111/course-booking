import React, { useContext, useEffect, useState } from 'react';

// react-bootstrap
import { 
    Button,
    Card, 
    Col, 
    Container, 
    Row 
} from 'react-bootstrap';

// router
import { useParams, Link } from 'react-router-dom';

// alert
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

const CourseView = () => {
    const [course, setCourse] = useState();
    const [btnLabel, setBtnLabel] = useState("Enroll");
    const [isDisabled, setIsDisabled] = useState(false);
    const { courseId } = useParams();

    const { user } = useContext(UserContext);
    
    useEffect(() => {
        // Fetch data
        fetch(`https://sheltered-reaches-76330.herokuapp.com/api/courses/${courseId}`, {
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(res => res.json())
            .then(res => {
                    setCourse(res)
                    if(course){
                        course.enrollees.forEach(course => {
                        if(course.userId === user.id){
                            setBtnLabel("Enrolled");
                            setIsDisabled(true);
                        } else {
                            setBtnLabel("Enroll");
                            setIsDisabled(false);
                        }
                    })
                }
            })
        }, [course, courseId, user.id])

        const enroll = () => {
            if(course.enrollees){
                course.enrollees.forEach(courseEnrollees => {
                    if(courseEnrollees.userId === user.id){
                        Swal.fire({
                            title: "Already enrolled",
                            icon: "error",
                            text: "You're currently enrolled to this course"
                        })
                    } 
                })
            } else {
                fetch("https://sheltered-reaches-76330.herokuapp.com/api/users/enroll", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization" : `Bearer ${localStorage.getItem("token")}`
                    },
                    body: JSON.stringify({
                        courseId: course._id
                    })
                })
                    .then(res => res.json())
                    .then(res => {
                        console.log(res);
                        if(res){
                            Swal.fire({
                                title: "Successfully enrolled",
                                icon: "success",
                                text: "You have successfully enrolled this course"
                            })
                        }
                    })
            }
        }

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card
                        style={{ 
                            width: "500px", 
                            height: "300px", 
                            margin: "0 auto",
                            textAlign: "center"
                        }}
                    >
                        <Card.Body>
                            <Card.Title>
                                {course?.courseName}
                            </Card.Title>
                            <Card.Subtitle>
                                Description:
                            </Card.Subtitle>
                            <Card.Text>
                                {course?.courseDesc}
                            </Card.Text>
                            <Card.Subtitle>
                                Price:
                            </Card.Subtitle>
                            <Card.Text>
                                PHP {course?.price}
                            </Card.Text>
                            <Card.Subtitle>
                                Class Schedule
                            </Card.Subtitle>
                            <Card.Text>
                                8AM - 5PM
                            </Card.Text>

                            {
                                user.id ? (
                                    <Button 
                                        className="btn-info"
                                        onClick={enroll}
                                        disabled={isDisabled}
                                    >
                                        {btnLabel}
                                    </Button>
                                ) : (
                                    <>
                                        <Link className="btn btn-info" to="/login"> Login to enroll</Link>
                                    </>
                                )
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default CourseView;
