import React, { useState, useEffect } from 'react';

// sweetalert
import Swal from "sweetalert2";

// User Context
// import UserContext from '../UserContext';

// react-bootstrap components
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Row 
} from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';

const Register = () => {
    // If already logged in redirect to /courses route
    // const { user } = useContext(UserContext);
    const history = useHistory();

    const token = localStorage.getItem("token")
    if(token !== null) {
        history.push('/courses');
    }

    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        mobile: "",
        password: "",
        confirm: ""
    });
    const [isDisabled, setIsDisabled] = useState(true)
    
    // Form Data destructuring
    const { firstName, lastName, email, mobile, password, confirm } = formData;
    
    // Use Effect
    useEffect(() => {
        if((firstName !== "" && lastName !== "" && email !== "" && mobile !== "" && password !== "" && confirm !== "")) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [firstName, lastName, email, mobile, password, confirm])
    

    // Events Handler Functions
    const handleChange = (e) => setFormData({...formData, [e.target.name]: e.target.value});

    const handleSubmit = (e) => {
        e.preventDefault();
        
        if(firstName.length < 3) {
            alert("First Name should be 3 or more letters")
        } else if(lastName.length < 3) {
            alert("Last Name should be 3 or more letters")
        } else if(email.length < 8) {
            alert("Invalid Email")
        } else if(mobile.length !== 11) {
            alert("Mobile Number must be 11 numbers")
        } else if(password.length < 8){
            alert("Password should be 8 or more letters")
        } else if(password !== confirm) {
            alert("Confirm Password and password does not match!");
        } else {
            fetch("https://sheltered-reaches-76330.herokuapp.com/api/users/register", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                }, 
                body: JSON.stringify({
                    firstName,
                    lastName,
                    email,
                    mobileNo: mobile,
                    password,
                })
            })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    if(res.bool) {
                        Swal.fire({
                            title: res.message, 
                            icon: "success",
                            text: "Welcome to Zuitt"
                        });
                        history.push('/login')
                    } else {
                        Swal.fire({
                            title: res.message, 
                            icon: "error",
                            text: "Please provide a different email"
                        });
                    }
                })
        }
    }

    return (
        <>
            {
                token ? <Redirect to="/courses" /> : (
                    <Container className="my-5">
                        <h1 className="text-center">Register</h1>
                        <Row className="justify-content-center mt-4">
                            <Col xs={10} md={6}>
                                <Form
                                    className="border p-3"
                                    onSubmit={handleSubmit}
                                >
                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="fName"
                                    >
                                        <Form.Label>First Name</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.firstName}
                                            name="firstName"
                                            type="text" 
                                            placeholder="Enter your First Name..." 
                                        />
                                    </Form.Group>

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="lName"
                                    >
                                        <Form.Label>Last Name</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.lastName}
                                            name="lastName"
                                            type="text" 
                                            placeholder="Enter your Last Name..." 
                                        />
                                    </Form.Group>

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="email"
                                    >
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.email}
                                            name="email"
                                            type="email" 
                                            placeholder="Enter email" 
                                        />
                                    </Form.Group>

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="mobile"
                                    >
                                        <Form.Label>Mobile Number</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.mobile}
                                            name="mobile"
                                            type="text" 
                                            placeholder="Enter your Mobile Number" 
                                        />
                                    </Form.Group>

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="password"
                                    >
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.password}
                                            name="password"
                                            type="password"
                                            placeholder="Password"
                                        />
                                    </Form.Group>

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="confirmPassword"
                                    >
                                        <Form.Label>Confirm Password</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.confirm}
                                            name="confirm"
                                            type="password" 
                                            placeholder="Confirm Password" 
                                        />
                                    </Form.Group>

                                    <Button 
                                        variant="primary" 
                                        type="submit"
                                        className="btn-info"
                                        disabled={isDisabled}
                                    >
                                        Submit
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                )
            }
        </>
    )
}

export default Register;