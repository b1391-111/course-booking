import React, { useContext, useEffect, useState } from 'react';

// data
import coursesData from '../data/courseData';

// react-bootstrap components
import { Container, Row } from 'react-bootstrap';

// Components
import UserContext from '../UserContext';
import AdminView from '../components/AdminView/AdminView';
import UserView from '../components/UserView/UserView';

const Courses = () => {
    const [courses, setCourses] = useState(coursesData);
    const { user } = useContext(UserContext);

    const fetchedData = () => { 
        fetch("https://sheltered-reaches-76330.herokuapp.com/api/courses")
            .then(res => res.json())
            .then(res => {
                setCourses(res);
            })
    }

    useEffect(() => {
        fetch("https://sheltered-reaches-76330.herokuapp.com/api/courses")
            .then(res => res.json())
            .then(res => {
                setCourses(res);
            })
    }, [])

    return (
        <Container className='my-5'>
            <h1 className="text-center">Courses</h1>
            <Row>
                {
                    user.isAdmin ? (
                        <AdminView 
                            courses={courses}
                            fetchedData={fetchedData}
                        />
                    ) : (
                        <UserView 
                            courses={courses}
                        />
                    )
                }
            </Row>
        </Container>
    )
}

export default Courses;