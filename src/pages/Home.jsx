import React from 'react'

// Components
import Banner from '../components/Banner/Banner';
import HighLights from '../components/Highlights/Highlights';

const Home = () => {
    return (
        <>
            <Banner 
                title="Zuitt Coding Bootcamp" 
                content="Opportunities for everyone, everywhere!"
                dest="/courses"
                btnLabel="Enroll Now!"    
            />
            <HighLights />
        </>
    )
}

export default Home
