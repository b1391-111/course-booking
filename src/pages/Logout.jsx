import React, { useContext } from 'react';

// user context
import UserContext from '../UserContext';

// React router
import { Redirect } from 'react-router-dom';

const Logout = () => {
    const { unsetUser } = useContext(UserContext);
    unsetUser();

    alert("Logging out!");

    return (
        <Redirect to="/login"/>
    )
}

export default Logout;